## 项目简介
BaseCloud是一套基于uniapp、uniCloud、uni-id的全栈开发框架，不依赖任何第三方框架，极度精简轻巧。

在开发前端界面时，除了适配移动端外，它对PC端也做了良好的适配；

在开发云函数时，它可以为您提供拦截器配置、路由管理、分页、列表、单数据快速查询等功能。除此之外，对于一些业务开发中的常用函数也已做好封装，拿来即用。

在BaseCloud的初始化项目模板中，为您实现了贯穿前后端的业务模块：管理员登录、用户管理、菜单管理、角色与权限管理、操作日志、系统参数配置等项目通用的基础后台管理功能，这一切全都基于云函数开发。

## 项目价值
基于BaseCloud的快速开发UI样式库，可以快速拼装前端界面，高还原度实现设计图效果，兼顾高效与灵活。

基于BaseCloud的云函数公用模块，你可以轻松实现单云函数、多云函数的路由管理、请求拦截管理与权限控制、常用业务函数快速开发。

基于BaseCloud的客户端缓存管理机制，你可以大幅度减少应用的云函数重复调用请求，未来云函数开始计费后，至少节省应用50%的流量费用。

基于BaseCloud的管理后台项目模板，你可以快速初始化一套自带用户、菜单、角色、权限、操作日志、系统参数管理的管理后台项目，在此基础上开始你的项目开发。

当然，这一切都只是刚刚开始，未来我们会基于BaseCloud推出更多贯穿前后端的业务模板，只要您的项目是基于BaseCloud框架，所有的业务模板拿来即用，5分钟快速集成到项目内，无需重复开发前端和后端。

对于开发者而言，基于BaseCloud的全栈快速开发框架，你可以封装自己的贯穿前后端的业务模块，发布到付费业务模块插件市场。

对于企业而言，基于BaseCloud的全栈快速开发框架，无需再费心招募不同工种不同技术栈的工程师，您只需要找到熟悉BaseCloud的工程师，让他们各自独立负责一个业务模块。 未来基于BaseCloud的项目，将会被拆分成几十个甚至上百个独立的模块，每个模块由一个工程师从前端到后端全链路负责，而他只需要懂一门开发语言：javascript，熟悉一个框架：BaseCloud。 项目交付后，您更无须担心后期维护与迭代更新，基于BaseCloud统一的开发规范，您可以很轻松找到随时能够接管您的项目的工程师。

[BaseCloud使用说明文档 << ](http://base-cloud.joiny.cn/docs/#/?isClient=base-cloud)

## 使用过程中如有问题，请加BaseCloud用户交流QQ群：

如果你想入手云开发，本框架是绝佳的学习素材和项目快速搭建方案，学习过程中有问题，快来群里提问，专业客服妹子秒回复。

群号：649807152

[点击链接，直接加入qq群 << ](https://qm.qq.com/cgi-bin/qm/qr?k=upb9fG80Wpsls_At8ZI01QTqDu_0KyUL&jump_from=webapi)

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-base-cloud/e699d5d0-ddd7-11ea-9dfb-6da8e309e0d8.png)

#业务模块快速集成 - 持续开发中

1. [用户端-手机号码登录业务模块 << ](https://ext.dcloud.net.cn/plugin?id=295)
2. [用户端-APP版本检测更新业务模块（含管理端功能） << ](https://ext.dcloud.net.cn/plugin?id=2510)

## BaseCloud项目构成

1. `common>base-cloud.scss` 基础样式库，适配移动端和PC端，22kb。
2. `common>js>base-cloud-client.js` 客户端SDK，14.2kb。
3. `cloudfunctions>common>base-cloud` 云函数公共模块，13.9kb。
4. `components` PC端常用业务组件目录

## 项目预览

1. 用户端APP版本更新业务模块：

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-base-cloud/20c85f40-ddd0-11ea-b244-a9f5e5565f30.gif)
![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-base-cloud/10a3f870-ddd7-11ea-8a36-ebb87efcf8c0.png)
![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-base-cloud/9ab75f30-ddd6-11ea-b997-9918a5dda011.png)

2. 用户端手机号登录业务模块：

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-base-cloud/cced6f10-dc48-11ea-b244-a9f5e5565f30.jpg)


3. 管理后台业务模块：

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-base-cloud/3d4a3910-dabd-11ea-b997-9918a5dda011.png)

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-base-cloud/0bcd2070-dac0-11ea-9dfb-6da8e309e0d8.png)

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-base-cloud/88657d30-dac0-11ea-b680-7980c8a877b8.png)

[管理后台演示项目地址：https://base-cloud.joiny.cn <<](https://base-cloud.joiny.cn)

账号：admin
密码：123123123

## 快速开始

1. 请先下载BaseCloud管理后台项目模板，并导入到Hbuilder中
2. 右键点击cloudfunctions目录，选择一个服务空间，支持阿里云、腾讯云。
3. 找到cloudfunctions目录下的db_init.json数据库初始化文件，右键选择“初始化数据库”。
4. 右键点击cloudfunctions目录，选择上传所有云函数以及公共模块。
5. 点击运行到浏览器，运行成功后，在浏览器中进入登录页，初始账号：admin ，初始密码：123123123
6. !!!!特别注意：如果您初次从插件市场导入项目，由于npm install创建的软链接失效，修改公共模块中的代码将不会同步更新到云函数中，
例如：修改`common > base-cloud > config.js` 中的拦截器配置信息，`amdin`函数中的`node_modules > base-cloud > config.js` 可能不会同步更改，
如果遇到这种情况请删除`admin > node_modules `和`admin > package-lock.json`,然后重新安装`admin`函数的公共模块：

>在要引入公用模块的云函数目录，执行npm init -y生成package.json文件。 执行npm install ../common/base-cloud引入base-cloud模块。

## 使用过程中如有问题或建议，请移步gitee提交issue。

[提交问题、建议](https://gitee.com/phoooob/BaseCloud/issues)

## 项目结构介绍

请务必对照仔细浏览项目目录介绍，您阅读本项目的文档将会事半功倍。

#### 服务端项目目录

```html
├── cloudfunctions───────────# 云函数目录
│ 		└── admin──────────────────# 管理后台业务函数
│			  └── controller──────────────────# 管理后台业务函数根目录
│					└── menu.js────────────────────────# 菜单管理业务函数
│					└── operateLog.js──────────────────# 操作日志业务函数与接口
│					└── paramConfig.js─────────────────# 系统参数配置业务函数
│					└── role.js────────────────────────# 角色管理业务函数
│					└── user.js────────────────────────# 用户管理业务函数
│			  └── node_modules──────────────────# admin函数依赖公共模块
│			  └── index.js──────────────────────# admin函数入口文件
│ 		└── api────────────────────# 用户端业务函数
│ 		└── clearlogs──────────────# 过期操作日志清理定时任务函数
│ 		└── common─────────────────# 公共模块
│				└── base-cloud──────────────────# base-cloud公共模块
│						└── intercepters──────────────────# 拦截器函数目录
│								└── authInter.js──────────────────# 用户权限拦截拦截函数
│						└── base-cloud-config.js──────────────────# 公共模块配置文件，注册全局拦截器（重要！）		
│						└── index.js──────────────────────# BaseCloud公共模块源码，开发阶段无需关心	
│		└── db_init.json───────────# 数据库初始化文件，包含数据表和初始化数据
```

#### 客户端项目目录

```html
├── cloudfunctions────────# 云函数目录...
├── common────────────────# 静态资源文件目录
│ 		└── js──────────────────# js文件目录
│			  └── base-cloud-client.js─────────────────# BaseCloud客户端SDK
│			  └── clipBoard.js─────────────────────────# 支持web端复制API
│			  └── md5.js───────────────────────────────# MD5加密函数，用于密码加密传输，客户端数据缓存等场景
│ 		└── base.scss────────────────────# BaseCloud样式类库入口文件
│ 		└── base-font.scss───────────────# BaseCloud图标样式文件
│ 		└── base-mobile.scss─────────────# BaseCloud移动端样式文件
│ 		└── base-pc.scss─────────────────# BaseCloud适配PC端样式文件
├── pages────────────────# 页面
├── static───────────────# 图片静态资源文件目录
├── uni.scss─────────────# scss变量配置文件
```

#### 管理后台业务模块云函数目录结构

```html
├── cloudfunctions─────────────────# 云函数目录
│ 		└── admin──────────────────# 管理后台业务函数
│				└── controller──────────────────# 管理后台业务函数根目录
│						└── menu.js────────────────────────# 菜单管理业务函数
│								└── listByType()─────────────────────# 根据菜单类型查询菜单列表接口
│								└── globalData()─────────────────────# 查询登录用户信息、权限菜单列表接口
│								└── info()───────────────────────────# 查询菜单信息接口
│								└── save()───────────────────────────# 保存、更新菜单信息接口
│								└── delete()─────────────────────────# 删除菜单信息接口
│								└── list()───────────────────────────# 菜单列表查询接口
│						└── operateLog.js──────────────────# 操作日志业务函数与接口
│						└── paramConfig.js─────────────────# 系统参数配置业务函数
│								└── info()───────────────────────────# 查询参数配置项信息接口
│								└── save()───────────────────────────# 保存、更新参数配置项信息接口
│								└── delete()─────────────────────────# 删除参数配置项接口
│								└── list()───────────────────────────# 参数配置项列表查询接口
│						└── role.js────────────────────────# 角色管理业务函数
│								└── info()───────────────────────────# 查询角色信息接口
│								└── save()───────────────────────────# 保存、更新角色信息接口
│								└── delete()─────────────────────────# 删除角色接口
│								└── list()───────────────────────────# 角色列表查询接口
│								└── options()────────────────────────# 角色选项列表查询接口（供用户角色选择时使用）
│						└── user.js────────────────────────# 用户管理业务函数
│								└── login()──────────────────────────# 登录接口
│								└── checkToken()─────────────────────# token验证接口
│								└── logout()─────────────────────────# 退出登录接口
│								└── changeStatus()───────────────────# 切换用户禁用状态接口
│								└── info()───────────────────────────# 用户信息查询接口
│								└── save()───────────────────────────# 保存、更新用户信息接口
│								└── myInfo()─────────────────────────# 当前用户信息接口
│								└── modify()─────────────────────────# 修改当前用户信息(含密码)接口
│								└── list()───────────────────────────# 用户列表查询接口
│								└── delete()─────────────────────────# 删除用户接口
│				└── node_modules──────────────────# admin函数依赖公共模块
│				└── index.js──────────────────────# admin函数入口文件
```


=====================================================================

## 服务端公共模块使用说明文档

#### 【使用公共模块来接管云函数，定义多个访问路径】

1. 根据[公共模块引入说明](https://uniapp.dcloud.io/uniCloud/cf-common)来引入base-cloud公共模块；
2. 在云函数的入口文件`index.js`中引入公共模块，并接管云函数。

```js
'use strict';
const BaseCloud = require("base-cloud");

exports.main = async ( event , ctx ) => {
	var fnName = "admin" ; //当前云函数的名称
	var controlerDir = `${__dirname}/controller` ; //存放业务函数根目录的绝对路径
	return await new BaseCloud({ event, ctx , fnName }).invoke(controlerDir);
};

```

3. 在云函数中指定的业务函数根目录（此处是controller，你也可以指定其他的目录），创建js文件。
并通过`module.exports`来导出业务处理的函数，可以导出一个或多个。
4. 客户端访问云函数的路径规则为：云函数名称/根目录下的js函数文件名称/js函数文件导出的函数名称；
如果js函数文件直接导出的是一个函数，则路径规则为：云函数名称/根目录下的js函数文件名称。

如下示例为在admin云函数中的`controller>operateLog.js`文件中导出一个函数：

```js
'use strict';
const db = uniCloud.database();
const dbCmd = db.command ;
const $ = db.command.aggregate ;
const OperateLog = db.collection("t_operate_log");

module.exports = async function(res){
	var {pageNumber , pageSize} = this.params ;
	
	var page = await this.paginate({
		pageNumber , pageSize ,
		collection : OperateLog ,
		eq : ["actionName","userName"],
		like : ["name"],
		orderBy : "createTime desc" 
	});
	
	var list = page.list ;
	list.forEach(item=>{
		item.createTime = this.DateKit.toStr( item.createTime ,'seconds');
	});
	
	return {page};
};
```

此时客户端访问路径为： `admin/operateLog` ，客户端调用示例如下：

```js
this.bcc.call({
	url : "admin/operateLog" ,
	data : {pageNumber: 1 , pageSize : 20},
	success : e=>{}
});
```

如下示例为在admin云函数中的`controller>role.js`文件中导出多个函数：

```js
'use strict';
const db = uniCloud.database();
const dbCmd = db.command ;
const $ = db.command.aggregate ;
const Role = db.collection("t_role");

module.exports = {
	info : async function(e){
		var id = this.params.id ;
		var typeList = TYPE_LIST ;
		if (!id) {
			return { typeList };
		}
		var data = this.findFirst( await Role.doc(id).get() );
		return { data , typeList };
	},
	
	save : async function(e){
		var data = this.getModel();
		if (data.menuIds) {
			data.menuIds = data.menuIds.split(',');
		}
		if (!data._id) {
			data.createTime = this.DateKit.now();
			await Role.add(data);
			return this.ok();
		}
		data.updateTime = this.DateKit.now() ;
		await this.updateById(Role , data);
		return this.ok();
	}
};
```

此时，通过`admin/role/info` 和 `admin/role/save` 两个路径可以分别访问 `controller>role.js>info()`
和`controller>role.js>save()`，客户端调用示例如下：

```js
this.bcc.call({
	url : "admin/role/info" ,
	data : {_id : 1},
	success : e=>{}
});
```

--------------------------------------------------

#### 【使用公共模块来配置全局的拦截器，以及拦截器的清理】
1. 在`cloudfunctions > common > base-cloud` 目录下，找到 `config.js` 文件，
2. 如下代码所示，在inters中配置了两个拦截器，你可以直接在此处定义拦截器函数（如loginInter），也可以通过文件引入的方式来定义拦截器（如authInter），具体的使用说明，请看注释：

```js
//通过文件来引入拦截器函数
const authInter = require("./intercepters/authInter") ;

module.exports = {
	isDebug : true , //会输出一些日志到控制台，方便调试
	inters:{ //配置全局拦截器
		loginInter: { //直接在此处定义拦截器函数
			isGlobal : true , //是否全局拦截，拦截所有的云函数，不配置默认是全局拦截
			handle : [] , //拦截的路径，此处留空表示拦截全部的路径
			clear : [ //配置要清除拦截器的路径，注意：如果配置了handle则此处的配置无效。
				"admin/user/login", //支持字符串、也支持正则表达式（详见示例项目中的authInter的配置规则）
				"admin/user/checkToken",
			] , 
			invoke:async function(attrs){//拦截器函数，入参为上一个拦截器通过setAttr方法传递的所有的键值对
				const {event , ctx , uniID } = this ;
				var res = await uniID.checkToken(event.uniIdToken);
				if(res.code){
					return {
						state : 'needLogin',
						msg : "请登录"
					};
				}
				//将user传入下一个拦截器，在拦截器函数的入参中可以获取到，也可以通过this.getAttr("user")来取到该值。
				this.setAttr({user : res.userInfo}); 
				//当前拦截器放行，不调用这个，拦截器不会放行，此次请求到此终止
				this.next();
			}
		},
		authInter  ,
	}
}
```

也就是说，你可以把你所有要配置的拦截器放到 `config.js > inters`中去，每个拦截器注册时，
通过`isGlobal`属性配置是否对所有云函数都拦截，不配置该属性默认为`true`；
在 `handle` 属性中定义要拦截的路径，路径支持正则表达式；
在`clear`属性中，定义要清理拦截器的路径；
使用`invoke`属性来定义拦截器的函数。
特别注意：如果在`handle`中定义了拦截的路径，则`clear`中的配置会被忽略。

在拦截器函数中，接收的参数为一个json，为所有拦截器通过`this.setAttr(key , value)`方法存入的键值对。

在拦截器中，可以使用`this.setAttr(key , value)`方法，将当前拦截器中的变量传递到下一个拦截器或者业务函数。
在下一个拦截器或业务函数的入参中可以接收，也可以使用`this.getAttr(key)`方法，来取到指定的值。

如果拦截器拦截成功，不再继续执行，直接返回响应结果即可。如果拦截器放行，则需要主动调用`this.next()`方法，来放行本次拦截。


>公共模块配置的拦截器对所有引入base-cloud公共模块，并由base-cloud接管的云函数都有效，请合理配置拦截与清理拦截的规则。

>修改公共模块后，除了上传公共模块，也需要上传依赖公共模块的云函数哦~

#### 在业务函数中配置拦截器的拦截和清除规则（v1.1.2新增）

在公共模块中配置拦截器，会全局生效。导致我们经常需要修改公共模块中的拦截器路由拦截规则，反复上传公共模块，极度影响开发效率。
当有新的路由拦截规则时，您可以选择在云函数入口文件中直接定义路由拦截规则，覆盖全局的拦截规则，参考代码如下：

```js
//云函数 api > index.js

/**
 * 用户端的API接口
 */
'use strict';
const BaseCloud = require("base-cloud");

exports.main = async ( event , ctx ) => {
	var baseCloud = new BaseCloud({ event, ctx , fnName : "api" });
	//配置云函数`api`的路由拦截规则：
	baseCloud.setInters({
		authInter : { //已在公共模块的config.js > inters 中注册的拦截器的函数名称
			clear : [/^api\//] //对所有api/开头的路径不执行authrInter拦截器的拦截
		}
	});
	return await baseCloud.invoke(`${__dirname}/controller`);
};

```

此处的代码，仅对`authInter`进行了局部配置，将覆盖`common > base-cloud > config.js` 中 `authInter` 的拦截规则；而`common > base-cloud > config.js` 中
`loginInter` 拦截器的拦截规则由于未进行局部配置，将按全局配置的拦截规则拦截所有的 `api云函数` 下的访问路径 。 


#### 【在业务函数中可以使用的变量】

还是以一个云函数中的业务函数为例：

```js
'use strict';
const db = uniCloud.database();
const dbCmd = db.command ;
const $ = db.command.aggregate ;
const OperateLog = db.collection("t_operate_log");

module.exports = async function(attrs){ //此处的attr是所有拦截器中通过 this.setAttr(key,value)方法存入的键值对
	var user = attrs.user ; //在loginInter拦截器中存入的user变量
	var ctx = this.ctx ; //上下文，为入口函数的入参context
	var event = this.event ; //为入口函数的入参event,本次请求云函数携带的event参数
	var params = this.params ; //本次请求客户端通过data或url传递所有的参数
	var fullPath = this.fullPath ; //本次请求的路径，如： admin/user/info
	var actionName = this.action ; //本次请求的action，不含云函数名称，如： user/info
	var fnName = this.fnName ; //本次请求的云函数的名称
	var token = this.token ; //本次请求携带的token
	var uniID = this.uniID ; //依赖的uniID模块，可以直接使用uniID的API
};

```

#### 【在业务函数中可以使用的方法】

### this.getModel(prefix , keepKeys) ;


1. 第一个参数为`prefix`参数，指定要获取的参数的前缀符，未指定时，默认为`x`。
2. 第二个参数为`keepKeys`，指定一个或多个键名进行接收，多个使用英文逗号分开，如未指定则接收所有带有指定前缀的参数。


举个例子，比如用户修改个人信息的功能，在客户端传参示例：

```js

this.bcc.call({
	url : "admin/user/modify" ,
	data : {
		"x.password" : "123123123" ,
		"x.mobile" : "15688585858" ,
		"x.realAuth.contact_name" : "王大成" ,
		"x.username" : "想改个名字能改吗" ,
		"remark" : "个人的喜好"
	}
})

```


此时我们需要只接收带`x.`前缀的参数，统一存放到`data`变量中，以便直接更新用户表的数据。
更新时，我们假设只允许用户修改`password`和`mobile`字段，不允许修改`username`字段，
那么使用 `this.getModel()` 方法可以获取到符合我们条件的参数。


### this.keep( jsonData , keepKeys) ;

保留jsonData中指定的键值对，用法同上。


```js

module.exports = async function(e){
	var data = this.getModel("x" , "mobile,password,realAuth");
};

```

### this.findFirst(dataInDB);

从数据库返回的结果中获取一条数据，如果没有数据则返回`null`

```js

var user = this.findFirst( await User.doc(id).get() );
if(null == user){
	return {} ;
}

var {username , mobile} = user ;
//...

```

### this.find(dataInDB);

从数据库返回的结果中获取列表数据，如果没有数据则返回 `[]` ;

```js

var dataInDB = await Role.orderBy("createTime","asc").get() ;
var list = this.find( dataInDB );
if(list.length == 0){
	//..do something
}


```

### async this.updateById( collection , updateData ) ;

根据主键_id来更新一条数据，`updateData`中包含`_id`字段和要更新的字段

```js

var Role = uniCloud.database().collection("t_role") ;
await this.updateById( Role , data);

```

### async this.paginate({ collection , where = {} , field = {} , pageNumber = 1, pageSize = 10 , orderBy , eq , like , gte , lte , gt , lt , range , rangeNeq , rangeReq , rangeLeq  });

使用数据库普通查询方法（暂不支持聚合查询）来获取分页数据。参数说明见下方的示例代码中的注释：

```js

'use strict';
const db = uniCloud.database();
const dbCmd = db.command ;
const $ = db.command.aggregate ;
const OperateLog = db.collection("t_operate_log");

module.exports = async function(res){
	var {pageNumber , pageSize , createTimeStart , createTimeEnd } = this.params ;
	
	this.params.createTimeStart = this.Datekit.parse(createTimeStart); //字符串的日期转时间戳
	this.params.createTimeEnd = this.Datekit.parse(createTimeEnd);
	
	var page = await this.paginate({
		pageNumber , //分页页码，不传入默认为1
		pageSize , //每页数据条数，不传入默认为10
		collection : OperateLog , //要查询数据的集合对象
		field:{ userName : true }, //指定返回字段，具体用法参见官方文档
		where:{},//自定义的固定查询条件
		//以下每个数组中的字符串均支持单个key和双key定义的方式
		eq : ["actionName","userName"], //筛选的相等条件，如果请求参数中该参数不为空则进行相等条件筛选
		like : ["name"],//筛选的模糊查询条件，如果请求参数中该参数不为空则进行模糊查询筛选
		gte : ["createTimeStart,createTime"], //如果this.params.createTimeStart值不为空，追加{createTime:dbCmd.gte(this.params.createTimeStart)}筛选条件
		lte : ["updateTime"],//如果this.params.updateTime值不为空，追加{updateTime:dbCmd.gte(this.params.updateTime)}筛选条件
		//范围类动态筛选条件：
		//比如根据日志创建日期范围来筛选日志的场景：用户端传入两个参数：createTimeStart、createTimeEnd；数据库中创建时间的字段名为createTime
		//如下定义，则如果createTimeStart不为空，追加{createTime:dbCmd.gte(this.params.createTimeStart)}筛选条件
		//如果createTimeEnd不为空，追加{createTime:dbCmd.lte(this.params.createTimeEnd)}筛选条件
		//如果createTimeStart、createTimeEnd都不为空，追加{createTime: dbCmd.gte(this.params.createTimeStart).and(dbCmd.lte(this.params.createTimeEnd)) }筛选条件
		range:["createTime,createTimeStart,createTimeEnd" , "updateTime" ],  //"createTime,createTimeStart,createTimeEnd"可以简写为"createTime"。"updateTime"代表"updateTime,updateTimeStart,updateTimeEnd"
		rangeNeq:["createTime,createTimeStart,createTimeEnd" , "updateTime" ], //用法同range，筛选条件为： min < x > max，不含左右等号
		rangeLeq:["createTime,createTimeStart,createTimeEnd" , "updateTime" ], //用法同range，筛选条件为： min <= x > max，不含右等号
		rangeReq:["createTime,createTimeStart,createTimeEnd" , "updateTime" ], //用法同range，筛选条件为： min < x >= max，不含左等号
		orderBy : "createTime desc" 
	});
	
	var list = page.list ;
	list.forEach(item=>{
		item.createTime = this.DateKit.toStr( item.createTime ,'seconds');
	});
	
	return {page};
};

```

以上查询足以应对大部分的筛选条件的业务场景，如果有一些个别的使用场景，可以自行创建where查询条件后，直接调用分页查询方法：

```js
//例如这样的跨字段操作：
var where = dbCmd.or(
  {
    type: {
      memory: dbCmd.gt(8)
    }
  },
  {
    type: {
      cpu: 3.2
    }
  }
);
var page = await this.paginate({
	pageNumber , pageSize ,
	collection : Computer ,
	where: where ,
	orderBy : "price desc" 
});
```

分页查询方法返回的数据结构如下：

```js

{
	pageNumber: 1,//页码
	lastPage: true,//是否最后一页
	totalPage: 1,//总页码
	list: [], //当前页数据
	totalRow: 0, //总数据条数
	pageSize: 10 //每页数据条数
}

```

### this.getPage({pageNumber , pageSize , totalRow , list , dataInDB });

|参数	|说明	|
|--	|--	|
|pageNumber	|	页码|
|pageSize	|	每页数据条数|
|totalRow	|	数据总条数|
|list	|	当前页数据列表，数组类型|
|dataInDB	|	当前页数据列表，数据库查询返回的未经处理的结果，JSON类型，未传入list时传dataInDB|

该方法返回标准的分页数据：
```js
{
	pageNumber: 1,//页码
	lastPage: true,//是否最后一页
	totalPage: 1,//总页码
	list: [], //当前页数据
	totalRow: 0, //总数据条数
	pageSize: 10 //每页数据条数
}
```

### this.ok(msg);

返回请求成功的响应结果，`msg`不传入时，默认提示信息为：

```js
{
	state : "ok" ,
	msg : "操作成功"
}
```

### this.fail(msg, state)

返回请求成功的响应结果，`msg`不传入时，默认提示信息为：

```js
{
	state : "fail" ,
	msg : "系统异常，请稍后再试"
}
```

### this.isRepeat( dataInDB , _id  );

做保存更新一体的业务接口时，我们经常会判断某个字段是否已在数据库中存在值。
先根据该字段查询数据中的一条数据，然后使用该方法，来快速判断是否有重复的值。

如下为保存更新用户数据接口代码示例：

```js

var data = this.getModel(); 
var {username , password , _id , roleIds , mobile } = data ;
var sameNameUser = this.findFirst(await User.where({username}).limit(1).get());
if(  this.isRepeat(sameNameUser , _id) ){
	return this.fail("用户名已存在");
}

```

### async this.setMaxOrderNum(data, collection, where ,step = 10);

适用于具有`orderNum`字段的数据表，自动生成最大的`orderNum`的业务场景。

1. `data` 参数为即将要保存、更新的json数据，必填
2. `collection`为要更新的集合对象，必填
3. `where`为限定排序的条件，可选项
4. `step` 为自增的步长，默认为10，方便自动生成后任意插入新的排序。

```js
var Menu = uniCloud.database().collection("t_menu");
var data = this.getModel();
await this.setMaxOrderNum(data , Menu , {parentId : data.parentId } );

console.log(data.orderNum) ; //输出最大的orderNum

```

### this.getTitleByValue(optionsArray , value);

在服务端我们经常需要定义多个键值对的数组，作为下拉列表的选项，如用户状态，通常我们会定义如下的数组常量：

```js
const STATUS = [
	{title : "正常" , value : 0 },
	{title : "禁用" , value : 1 },
	{title : "待审核" , value : 2 },
	{title : "审核拒绝" , value : 3 },
] ;
```

此时，直接作为数组返回给客户端，客户端使用 `radios` 或 `selects` 组件来供用户选择使用，一般我们会把`value`存入数据库，当向用户展示时，我们需要根据`value`
找到对应的`title`，此时可以使用`this.getTitleByValue()`方法：
```js
var status = user.status ; // 0,1,2,3
var statusTitle = this.getTitleByValue(STATUS , status); //正常、禁用、待审核、审核拒绝
```

### this.log(...arguments);

方便调试的方法，如果在 `base-cloud > config.js ` 中配置了 `isDebug` 参数为 `true` ，使用该方法时，可以输出日志，否则不输出日志。

### this.isNull(obj);

判断是否为空

### this.isObject(obj);

判断是否为json对象

### this.isEmptyObject(obj);

判断是否值为{}的json对象，不含有任何键值对的json

### this.isFn(fn);

判断是否为函数

### this.isNumber(number);

判断是否为数字

### this.isArray(array);

判断是否为数组

### this.isString(string);

判断是否为字符串

### this.isDate(date);

判断是否为日期类型

### this.isReg(reg);

判断是否为正则表达式

### this.Datekit.now()

uniCloud服务器默认是0时区的时间，使用该方法可以获取东八区当前时间的时间戳，符合国内的习惯。如果使用该方法创建的时间戳存入数据库，
那么返回给用户端之前，需要在服务端直接转成日期字符串，在用户端转换字符串会造成时区差的问题。

### this.DateKit.addMinutes(minutes , date);

date为可选参数，时间戳类型，不传入则使用东八区的当前时间时间戳，增加或减少指定分钟数量，返回时间戳。

### this.DateKit.addHours(hours , date);

date为可选参数，时间戳类型，不传入则使用东八区的当前时间时间戳，增加或减少指定小时数量，返回时间戳。

### this.DateKit.addDays(days , date);

date为可选参数，时间戳类型，不传入则使用东八区的当前时间时间戳，增加或减少指定天数，返回时间戳。

### this.DateKit.addMonths(months , date);

date为可选参数，时间戳类型，不传入则使用东八区的当前时间时间戳，增加或减少指定月份，返回时间戳。

### this.Datekit.toStr( timestamp , fileds );

传入一个时间戳时间，格式化为字符串，`fileds` 来指定格式化的时间精度，支持：second、minute、hour、day、month、year，不传默认为minute

### this.DateKit.friendlyDate(timestamp);

传入一个时间戳时间，返回距离东八区当前时间的有多少天、时、分、秒。如：3天前、24分钟后

--------------------------------------------------

#### 【服务端响应结果的约定】

一般操作类的接口，服务端会返回如下结果：

```js
{
	state : 'ok' ,
	msg : "操作成功"
}
```

数据查询类的，正常情况下，服务端不再返回state、msg字段，直接返回要查询的数据结果，如：

```js
{
	list : [],
	data :{}
}
```

|state	|说明	|
|--	|--	|
|ok	|请求成功	|
|fail	|请求失败，失败时需要返回msg字段，作为失败的说明信息	|
|noAuth	|无操作权限	|
|needLogin	|需要登录	|
|...	|其他特殊场景下的状态描述	|


=====================================================================

## 客户端SDK使用说明文档

使用客户端sdk前，请确保已按如下方式，在 `main.js` 中注册全局对象。

```js
import Vue from 'vue'
import App from './App'

import bcc from "./common/js/base-cloud-client.js" //引入客户端sdk文件
Vue.prototype.bcc = bcc ; //注册为全局对象

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()

```

#### 【调用云函数】

```js
var data = e.detail.value ;
this.bcc.call({
	debug : true ,
	url : 'admin/user/save' , //请求路径，直接以函数名称开头，开头不要加/，后面跟着路径
	data : data , //请求参数
	success : res => { 
		//当服务端返回state == 'ok' 或无state字段时，进入success回调
	},
	fail : res => { 
		//当服务端返回state == 'fail' 时，进入fail回调；
		//如未定义fail回调，则默认提示服务端返回的msg字段
	},
	complete : res => {
		//请求完成后的回调
	},
});
```

  `debug:true`，开启调试模式后，将在控制台输出请求的路径和参数（仅开发模式下有效，编译发布后不会输出），提升开发调试效率。
```js
this.bcc.call({
	debug : true //默认为true，如果不需要控制台输出日志信息，请配置为false
})
```
  开启开发模式后，发送请求时，将在浏览器控制台输出日志信息，方便我们快速开发调试：
```js
  base-cloud-client.js:15 ======================
  base-cloud-client.js:16 云函数： admin , action ： user/login , data ：{"username":"admin","password":"2255BE0951400E260832C85C5D191247"}
  base-cloud-client.js:17 {username: "admin", password: "2255BE0951400E260832C85C5D191247"}
  base-cloud-client.js:18 ======================
```

默认情况下，表单数据一般都会以字符串的形式提交，直接存入数据库会给我们带来麻烦，因为MongoDB查询数据库时，对数据类型是敏感的。基于这点 `base-cloud` 客户端sdk会在发送请求之前，对提交的参数按会可能的类型进行转换，
这点也会导致一些意外的情况，如果不需要自动转换的，可以做如下配置：
```js
  this.bcc.call({
  	 keepStr : true , //将所有参数都不进行类型转换
	 keepKeys : "password,idcard,mobile" //指定不进行转换的参数名，多个用英文逗号分开。
  })
```
	以上两个配置项，可以选择使用，针对一些意外情况的发生进行处理。
	
	对应的，如果使用 `this.bcc.submit()`方法，自动提交表单，则需要在 `form` 标签定义 `data-keepStr` 、 `data-keepKeys` 属性：
	
```html
//指定键名不进行类型转换，键名无需包含校验规则，多个用逗号分开
<form @submit="submit" data-action="admin/user/save" data-keepKeys="x.name,x.remark" data-back="/pages/user/userList" data-clear="admin/user/list">

	<inputs name="x.name|用户名" title="用户名" :value="data.name"></inputs>
	
	<inputs name="x.mobile|联系电话" title="联系电话" :value="data.mobile"></inputs>

	<labels class="mt40">
		<button class="btn greenBg w80" form-type="submit">{{ !data._id ? '保存' : '修改'}}</button>
	</labels>
</form>

//指定所有的参数都不进行类型转换
<form @submit="submit" data-action="admin/user/save" data-keepStr="true" data-back="/pages/user/userList" data-clear="admin/user/list">

	<inputs name="x.name|用户名" title="用户名" :value="data.name"></inputs>
	
	<inputs name="x.mobile|联系电话" title="联系电话" :value="data.mobile"></inputs>

	<labels class="mt40">
		<button class="btn greenBg w80" form-type="submit">{{ !data._id ? '保存' : '修改'}}</button>
	</labels>
</form>

```
```js
methods:{
	submit : function(e){
		this.bcc.submit(e);
	}
}
```

#### 【通过客户端本地缓存调用云函数】

>本地没有缓存则请求云函数数据并将请求到的数据缓存至本地，否则直接使用本地缓存数据。

```js
var data = e.detail.value ;
this.bcc.callInCache({
	url : 'admin/user/list' , //请求路径，直接以函数名称开头，开头不要加/，后面跟着路径
	data : data , //请求参数
	success : res => {
		//当服务端返回state == 'ok' 或无state字段时，进入success回调，并且将数据缓存至本地
	},
	fail : res => { 
		//当服务端返回state == 'fail' 时，进入fail回调；
		//如未定义fail回调，则默认提示服务端返回的msg字段
	},
	complete : res => {
		//请求完成后的回调
	},
});
```

#### 【主动清理客户端本地缓存】
>当本地数据已经发生变更时，需要主动清理本地缓存数据，下次去请求最新的数据。

>清理缓存时，将直接清理所有指定请求地址下的缓存（一个请求地址下，因参数不同可能会有多个本地缓存数据）。

应用场景示例：将用户列表数据存入本地缓存，当编辑用户信息、删除用户数据、更改用户状态等三个操作发生时。直接清理本地的用户列表数据缓存：

```js
//传入要清理缓存的请求地址的路径
this.bcc.clearCache("admin/user/list");
```

#### 【表单数据校验】

>表单校验无须配置各种校验规则，直接将校验规则写入name即可。

>表单的name一共分成四个部分，用|符号分割：

1. 要传入服务端的name
2. 表单的标题，如果标题为空表示该表单可以为空。标题内可以含有：请输入、请上传、请选择 这三类提示文字。
3. 校验规则，目前支持：mobile、email、idcard、count（整数）、amount(金额)、字符长度与长度范围等六种常见的表单验证和非空验证。
4. empty ：表示可以为空，如果有值则进行校验，无值则放行。

示例代码：

```html
/* 要传入服务端的name为：x.name ，角色名称不可为空，校验规则为：字符长度2~20之间。 */
<inputs name="x.name|角色名称|2~20" title="角色名称" :value="data.name"></inputs>

/* 要传入服务端的name为：x.type ，该字段可以为空 */
<radios title="类型" name="x.type" :list="typeList" :value="data.type"></radios>

/* 要传入服务端的name为：x.remark ，该字段可以为空, 如果有值时，校验字符长度在2~200之间*/
<textareas title="角色描述" name="x.remark|角色描述|0~200|empty" 
:value="data.remark?data.remark:''" :maxlength="200" placeholder="选填"></textareas>

/* 要传入服务端的name为：x.mobile ，校验手机号码*/
<inputs name="x.mobile|联系电话|mobile" title="联系电话" :value="data.name"></inputs>

/* 要传入服务端的name为：code ，校验规则为长度为6的字符*/
<inputs name="code|验证码|6" title="验证码"></inputs>

/* 要传入服务端的name为：avatar ，未上传头像提交表单时会提示：请上传头像*/
<upload-images name="avatar|请上传头像" title="头像"></upload-images>
```

#### 【提交表单数据】

>直接将@submit接收到的参数e，传递给this.bbc.submit()函数即可，自动做表单校验，验证通过后，提交表单。

>通过给form配置data-action属性来定义表单提交的地址；

>给form配置data-back来定义请求成功后返回的页面的地址；

>给form配置data-confirm来定义提交表单之前的确认弹窗的文字，未定义则不显示确认弹窗；

>给form配置data-alert来定义提交表单成功以后的弹窗的文字，未定义则不显示弹窗；

>给form配置data-redirect来定义请求成功后跳转的页面的地址；

>给form配置data-clear来定义请求成功后清理本地缓存的请求url（不可含参数）。

vue:

```vue

<form @submit="submit" data-action="admin/role/save" data-back="/pages/role/roleList" data-clear="admin/role/list">

	<inputs name="x.name|角色名称" title="角色名称" :value="data.name"></inputs>
	
	<radios title="类型" name="x.type" :list="typeList" :value="data.type"></radios>
	
	<textareas title="角色描述" name="x.remark|角色描述|empty" 
	:value="data.remark?data.remark:''" :maxlength="200" placeholder="选填"></textareas>
	
	<labels :isTop="true" title="权限配置">
		<menu-groups :list="menuList" name="x.menuIds" :value="data.menuIds"></menu-groups>
	</labels>
	
	<labels class="mt40">
		<inputs type="hidden" name="x._id" :value="id" v-if="id"></inputs>
		<button class="btn greenBg w80" form-type="submit">{{ !data._id ? '保存' : '修改'}}</button>
		<button class="btn grayBg line w80" @click="bcc.goBack()">取消</button>
	</labels>
	
</form>
```

js：

```js
methods: {
	submit:function(e){
		this.bcc.submit(e , res=>{
			//如果配置了第二个参数：成功回调函数，则当服务端返回state == 'ok' 或无state字段时，进入请求成功的回调
		}, err=>{
			//如果配置了第三个参数：失败回调函数，则当服务端返回state == 'fail' 时，进入fail回调
		});
	}
}
```

#### 【表单数据主动验证】

>通过@submit接收到参数e后，直接用该参数进行表单验证。

```js
submit:function(e){
	var res = this.bcc.checkData(e);
	if (res.fail) { //表单校验未通过
		return ;
	}
	uni.showLoading({
		title:"请稍后…",
		mask:true 
	});
	var data = res.data ; //表单校验通过后拿到要向服务端提交的处理过的数据
	if (data['x.password']) {
		data['x.password'] = this.bcc.sign(data['x.password']);
	}
	this.bcc.call({
		url : 'admin/user/save' ,
		data : data ,
		success : res => {
			uni.hideLoading();
			this.bcc.clearCache("admin/user/list");
			this.bcc.goSuccessBack("/pages/user/userList","保存成功");
		}
	});
},
```

#### 【辅助工具类】

```js

//向后返回2层页面，如果页面栈不足2个页面的话，就返回到/pages/index/index
this.bcc.goBack("/pages/index/index",2);

//返回上一历史页，如果上一历史页不存在则返回/pages/user/userList，然后提示“保存成功”
this.bcc.goSuccessBack("/pages/user/userList","保存成功");

//MD5加密字符串
var sign = this.bcc.sign('string...');

//判断某个变量是否为空
var isNull = this.bcc.isNull(a);

//判断某个变量是否是数字
var isNumber = this.bcc.isNumber(a);

```



=====================================================================

## 基础样式类库使用说明

基于BaseCloud的基础样式类库，您可以高度还原UI设计图，快速搭建客户端界面。与传统UI框架不同的是，
并不直接定义任何界面组件，它通过对高频基础样式类的封装，使用自由搭配组合的class样式类来快速、随心所欲的呈现千变万化的UI界面。

我们希望每个项目都有统一的主题色，以保证项目的界面的干净整洁，故而定义了一个主题色，你可以在`uni.scss`中，修改成自己喜欢的颜色，作为整个项目的主题色。
同时，由于部分组件的自定义颜色属性，我们也使用了默认主题色，所以，如果您有修改主题色的需求，
除了修改`uni.scss`中的主题色色值外，还可以在Hbulider中使用`ctrl + alt + F`快捷键，全项目搜索该色值并替换。

>uni.scss
``` css
$main:#07c160;  //主背景色
$lightMain: #dff5e2;  //淡主色
$mainInverse:#fff;  //与主色搭配的反色
$mainGradual:linear-gradient(to top right,#67D79F,#00A28A); //渐变主色
$mainGradualInverse:#fff; //与渐变主色搭配的反色
```

修改完毕后，请确保在 `App.vue` 文件中通过如下方式引入样式库文件：

```css
App.vue

<script>
	export default {
		onLaunch: function() {
			console.log('App Launch')
		},
		onShow: function() {
			console.log('App Show')
		},
		onHide: function() {
			console.log('App Hide')
		}
	}
</script>

<style lang="scss">
	@import './common/base-cloud.scss';
</style>

```

目前移动端的组件还没有，后续视情况会逐步增加。建议学习一下base-cloud样式类库，可以快速拼装任何设计图界面，非常非常非常，非~常~强大，常用的纯静态界面模块一般都不需要组件。

样式类库详细使用说明文档，请点击此处链接查看：
[《UI样式类库详细使用使用文档》](https://fastshop.joiny.cn/docs)

特别需要说明的是，基于该基础样式类库，您可以快速构建任何UI界面。
项目初始，目前我们仅对PC端一些常用组件进行了封装，供您使用，后续随着贯穿前后端的业务模块的开发，我们会逐步提供更多的组件。

=====================================================================

## PC端组件使用说明文档

#### 【auth 组件】

>用于用户权限控制，当用户拥有操作权限时展现，否则不展现该元素。

>关于权限控制的业务逻辑：用户登录成功后，读取该用户所属角色拥有的权限菜单列表，存储到本地，键名为menuList，权限判断就是基于menuList进行的判断。

|属性	|必填	|默认值	|可选值	|示例值	|说明	|
|--	|--	|--	|--	|--	|--	|
|url	|是	|无	|	|admin/user/save	|权限路径,该路径可包含参数，需在t_menu表中已添加数据	|
|noAuth	|否	|false	|true|false	|无权限时展现	|
|isInline	|否	|true	|true|false	|是否内联元素	|

```html
<auth url="admin/user/save">

   <navigator url="/pages/user/userEdit">编辑</navigator>	
   
</auth>
```


#### 【auth-btn 组件】


>用于用户权限控制，当用户拥有操作权限时展现，否则不展现该元素。点击按钮时，会发送请求。


|属性	|必填	|默认值	|可选值	|示例值	|说明	|
|--	|--	|--	|--	|--	|--	|
|url	|是	|无	|	|admin/user/changeStatus?id=1	|发送请求的路径，可以携带参数	|
|params	|否	|无	|json类型	| :params="{id:1}"	|发送请求时携带的参数	|
|noAuth	|否	|false	|true|false	|无权限时展现	|
|isInline	|否	|true	|true|false	|是否内联元素	|
|confirm	|否	|无	||confirm="delete"	|发送请求之前的确认文字，如果是删除类请求需要确认，可以简写为delete	|
|alert	|否	|无	||	|请求成功后弹窗的文字	|
|showFail	|否	|true	||	|请求失败后，是否提示服务端返回的msg字段	|
|@success	|否	|无	||	|请求成功后回调函数	|
|@fail	|否	|无	||	|请求失败后回调函数	|

```html
<auth-btn url="admin/user/changeStatus" :params="{id:1}">
   禁用
</auth-btn>
```

#### 【auth-nav 组件】


>用于用户权限控制，当用户拥有操作权限时展现，否则不展现该元素。点击按钮时，会进行页面跳转。


|属性	|必填	|默认值	|可选值	|示例值	|说明	|
|--	|--	|--	|--	|--	|--	|
|url	|是	|无	|	|admin/user/changeStatus?id=1	|是否具有权限的路径	|
|noAuth	|否	|false	|true|false	|无权限时展现	|
|isInline	|否	|true	|true|false	|是否内联元素	|
|href	|是	|无	|  | 	|要跳转的页面的链接，可以包含参数	|

```html
<auth-nav :href="`/pages/user/userEdit?id=${item._id}`" url="admin/user/save" >
   编辑
</auth-nav>
```

#### 【switch-btn 组件】
>用于权限控制的开关切换按钮，无权限仅展示，不可发送请求。

|属性	|类型	|说明	|
|--	|--	|--	|
|url	|String	|权限地址，也是点击切换时的请求地址，可以携带参数，如无地址或无权限，则不可点击	|
|params	|json	|请求参数，有权限时，点击切换即可发送请求	|
|checked	|Boolean	|开关是否打开	|
|disabled	|Boolean	|开关是否禁用	|
|color	|String	|颜色，默认#07c160	|

#### 【switchs 组件】
|属性	|类型	|说明	|
|--	|--	|--	|
|name	|String	|表单的name|
|value	|Boolean	|开关是否打开	|
|tip	|String	|开关右侧的提示文字	|
|disabled	|Boolean	|开关是否禁用	|
|color	|String	|颜色，默认#07c160	|
|title	|否	|	|	|	|表单标题，不要标题，请设置titleWidth=0	|
|titleWidth	|否	|90	|数值即可	|100	|表单的左侧标题占位的宽度	|
|isVertical	|否	|false	|	|	|标题和开关是否垂直排列	|

#### 【checkboxs 组件】

>复选框组件，用于多选，支持v-model

|属性	|必填	|默认值	|可选值	|示例值	|说明	|
|--	|--	|--	|--	|--	|--	|
|title	|否	|	|	|	|表单标题，不要标题，请设置titleWidth=0	|
|titleWidth	|否	|90	|数值即可	|100	|复选框表单的左侧标题占位的宽度	|
|name	|是	|	|	|	|表单的name	|
|titleName	|否	|	|	|	|如果需要选中选项的标题也传服务端，请定义该字段	|
|value	|否	|	|	|value='1,2,3,5'	|表单的value，支持v-model绑定，可以是数组，也可以是用英文逗号分开的多个值	|
|list	|是	|	|	|[{title:"搞笑",value:1},{title:"言情",value:2}]	|选项列表，数组	|
|titleKey	|否	|title	|	|	|选项列表中，对用户展示的文字的键值对的键名	|
|valueKey	|否	|value	|	|	|选项列表中，对作为选项值的键值对的键名	|
|disabledKey	|否	|disabled	|	|	|选项列表中，表示当前选项禁用的键值对的键名	|
|color	|否	|#07c160	|	|	|复选框的颜色	|
|isVertical	|否	|false	|	|	|标题和复选框是否垂直排列	|
|@change	|	|	|	|	|当选项发生改变时触发的回调函数	|

```html
<checkboxs title="角色" :list="roleList" name="x.roleIds|请选择角色" :value="data.roleIds" 
titleName="x.roleNames" titleKey="name" valueKey="_id"></checkboxs>
```

#### 【radios 组件】

>单选框组件，用于单选，支持v-model

|属性	|必填	|默认值	|可选值	|示例值	|说明	|
|--	|--	|--	|--	|--	|--	|
|title	|否	|	|	|	|表单标题	|
|titleWidth	|否	|90	|数值即可	|100	|左侧标题的宽度	|
|name	|是	|	|	|	|表单的name	|
|titleName	|否	|	|	|	|如果需要选中选项的标题也传服务端，请定义该字段	|
|value	|否	|	|	|	|表单的value，支持v-model绑定	|
|list	|是	|	|	|[{title:"搞笑",value:1},{title:"言情",value:2}]	|选项列表，数组	|
|titleKey	|否	|title	|	|	|选项列表中，对用户展示的文字的键值对的键名	|
|valueKey	|否	|value	|	|	|选项列表中，对作为选项值的键值对的键名	|
|disabledKey	|否	|disabled	|	|	|选项列表中，表示当前选项禁用的键值对的键名	|
|color	|否	|#07c160	|	|	|复选框的颜色	|
|isVertical	|否	|false	|	|	|标题和复选框是否垂直排列	|
|defaultFirst	|否	|true	|	|	|当value无值时，是否默认选中第一个选项	|
|@change	|	|	|	|	|当选项发生改变时触发的回调函数	|

```html
<radios title="菜单类型" :list="menuTypeList" :value="data.type" 
name="x.type|菜单类型" @change="chooseMenuType"></radios>
```

#### 【multi-selects 组件】

>下拉多选组件，用于多选，支持v-model，可以搜索关键字筛选

|属性	|必填	|默认值	|可选值	|示例值	|说明	|
|--	|--	|--	|--	|--	|--	|
|title	|否	|	|	|	|表单标题	|
|titleWidth	|否	|90	|数值即可	|100	|左侧标题的宽度	|
|name	|是	|	|	|	|表单的name	|
|titleName	|否	|	|	|	|如果需要选中选项的标题也传服务端，请定义该字段	|
|value	|否	|	|	|value='1,2,3,5'	|表单的value，支持v-model绑定,可以是数组，也可以是用英文逗号分开的多个值	|
|list	|是	|	|	|[{title:"搞笑",value:1},{title:"言情",value:2}]	|选项列表，数组	|
|titleKey	|否	|title	|	|	|选项列表中，对用户展示的文字的键值对的键名	|
|valueKey	|否	|value	|	|	|选项列表中，对作为选项值的键值对的键名	|
|remarkKey	|否	|remark	|	|	|选项列表中，对作为副标题的键值对的键名	|
|disabledKey	|否	|disabled	|	|	|选项列表中，表示当前选项禁用的键值对的键名	|
|color	|否	|#07c160	|	|	|颜色	|
|isVertical	|否	|false	|	|	|标题和选择框是否垂直排列	|
|@change	|	|	|	|	|当选项发生改变时触发的回调函数	|

```html
<multi-selects title="角色" :list="roleList" name="x.roleIds|请选择角色" 
:value="data.roleIds" titleName="x.roleNames" titleKey="name" valueKey="_id"></multi-selects>
```


#### 【selects 组件】

>下拉单选组件，用于单选，支持v-model，可以搜索关键字筛选

|属性	|必填	|默认值	|可选值	|示例值	|说明	|
|--	|--	|--	|--	|--	|--	|
|title	|否	|	|	|	|表单标题	|
|titleWidth	|否	|90	|数值即可	|100	|左侧标题的宽度	|
|name	|是	|	|	|	|表单的name	|
|titleName	|否	|	|	|	|如果需要选中选项的标题也传服务端，请定义该字段	|
|value	|否	|	|	|	|表单的value，支持v-model绑定	|
|list	|是	|	|	|[{title:"搞笑",value:1},{title:"言情",value:2}]	|选项列表，数组	|
|titleKey	|否	|title	|	|	|选项列表中，对用户展示的文字的键值对的键名	|
|valueKey	|否	|value	|	|	|选项列表中，对作为选项值的键值对的键名	|
|remarkKey	|否	|remark	|	|	|选项列表中，对作为副标题的键值对的键名	|
|disabledKey	|否	|disabled	|	|	|选项列表中，表示当前选项禁用的键值对的键名	|
|color	|否	|#07c160	|	|	|颜色	|
|isVertical	|否	|false	|	|	|标题和选择框是否垂直排列	|
|@change	|	|	|	|	|当选项发生改变时触发的回调函数	|

```html
<selects title="父级菜单" :list="parentMenuList" name="x.parentId" 
:value="data.parentId" titleKey="name" valueKey="_id"></selects>
```

#### 【inputs 组件】

>输入框组件，type支持hidden类型，输入框有内容时，可以点击清空图标清空。

|属性	|必填	|默认值	|可选值	|示例值	|说明	|
|--	|--	|--	|--	|--	|--	|
|title	|否	|	|	|	|表单标题	|
|titleWidth	|否	|90	|数值即可	|100	|左侧标题的宽度	|
|name	|是	|	|	|	|表单的name	|
|value	|否	|	|	|	|表单的value，支持v-model绑定	|
|hiddenValue	|否	|	|	|	|传入该值时，输入框将变为禁用状态，对用户展示value的值，hiddenValue将会传到服务端	|
|type	|否	|text	|text、number、hidden	| |表单类型，支持hidden	|
|addOn	|否	|	|	|	|输入框右侧的文字块的文字	|
|addOnLeft	|否	|	|	|	|输入框左侧的文字块的文字	|
|isVertical	|否	|false	|	|	|标题和输入框是否垂直排列	|
|showClearIcon	|否	|true	|	|	|是否显示清空图标	|
|@tapAddOn	|	|	|	|	|当点击输入框右侧文字块时触发的回调函数	|
|@tapAddOnLeft	|	|	|	|	|当点击输入框左侧文字块时触发的回调函数	|
|其他属性与事件	|	|	|	|	|与input组件一致	|

```html
<inputs name="x.name|用户名" title="用户名" :value="data.name" :hiddenValue="data._id"></inputs>
```

#### 【textareas 组件】

|属性	|必填	|默认值	|可选值	|示例值	|说明	|
|--	|--	|--	|--	|--	|--	|
|title	|否	|	|	|	|表单标题	|
|titleWidth	|否	|90	|数值即可	|100	|左侧标题的宽度	|
|name	|是	|	|	|	|表单的name	|
|value	|否	|	|	|	|表单的value，支持v-model绑定	|
|isVertical	|否	|false	|	|	|标题和文本框是否垂直排列	|
|showClearIcon	|否	|true	|	|	|是否显示清空图标	|
|autoHeight	|否	|false	|	|	|是否自适应高度	|
|height	|否	|100	|	|	|非自适应高度时的高度	|
|其他属性与事件	|	|	|	|	|与textarea组件一致	|

```html
<textareas title="权限地址" @blur="inputBlur" name="x.url|权限地址" 
:value="data.url" placeholder="多个权限地址请用英文分号隔开"></textareas>
```

#### 【datepicker 组件】

|属性	|类型	|说明	|
|--	|--	|--	|
|showIcon	|Boolean	|是否显示右侧的日历图标，默认true	|
|name	|String	|表单的name	|
|endName	|String	|表单的endName，如果是时间范围，表示结束时间的name	|
|value	|String,Array	|默认选中的日期，也是提交表单时的默认值，支持数组，如果是字符串的话，可以是用英文逗号分开的两个时间（开始和结束时间）	|
|name	|String	|表单的name	|
|placeholder	|String	|未选择日期时，显示的文字	|
|color	|String	|日历的颜色	|
|showSeconds	|Boolean	|是否显示秒	|
|type	|String	|时间类型：rangetime、range、time、date、datetime	|
|format	|String	|初始日期格式	|
|showHoliday	|Boolean	|显示公历节日	|
|showTips	|Boolean	|显示提示	|
|beginText	|String	|开始时间的文字，默认为：开始	|
|endText	|String	|结束时间的文字，默认为：结束	|
|title	|String|表单标题	|
|titleWidth	|Number|左侧标题的宽度	|
|isVertical	|Boolean|标题和文本框是否垂直排列	|

#### 【conditions 组件】

>分页筛选条件组件

|属性	|必填	|默认值	|可选值	|示例值	|说明	|
|--	|--	|--	|--	|--	|--	|
|list	|否	|	|	|{title:"用户名",name:"name"},{title:"状态",name:"status",type:"select",list:[]}	|筛选条件,数组，基本属性为：name、title、type、list，详见/pages/user/userList示例	|
|conditions	|否	|{}	|json	|	|当前的筛选条件	|
|confirmText	|否	|筛选	|	|	|筛选按钮的文字	|
|showFirst	|否	|false	|	|	|是否对外显示第一个筛选项	|
|showCount	|否	|0	|	|	|对外显示前面多少个筛选项	|
|@confirm	|	|	|	|	|确认筛选时的回调事件，`e.conditions`	|

```html
<conditions :conditions="conditions" :list="conditonList" @confirm="submitSearch"></conditions>
```

```js
data() {
	return {
		conditonList:[
			{title:"用户名",name:"name"}, //默认是输入框类型的，只需提供这两个属性即可
			//如果是下拉选择类型的，则需要提供list属性，两个键值对：title、value
			{title:"状态",name:"status",type:"select",list:[{title:"正常",value:0},{title:"禁用",value:1}]}, 
			//日期类型的筛选条件：可以配置name和endName，表示开始和结束时间，timeType是指日期格式：
			/**
			 * rangetime 时间范围，精确到分钟
			 * range 时间范围，精确到天
			 * time 时间，小时：分钟
			 * date 日期，年-月-日
			 * datetime 日期时间，精确到分钟
			 */
			//showSeconds属性表示是否显示秒，任何日期格式都可以搭配这个属性使用。
			{title:"时间",name:"startTime",endName:"endTime",type:"time",timeType:"rangetime",showSeconds:true},
		],
		conditions:{
			name : ""
		}
	}
},
```

#### 【copy 组件】

>一键复制的功能

|属性	|说明	|
|--	|--	|
|text	|要复制的文字内容	|
|showIcon	|文字右侧是否显示复制图标，默认true	|

```html
<copy :text="data.text" :showIcon="false"></copy>
```

#### 【empty 组件】

|属性	|类型	|说明	|
|--	|--|--	|
|list	|Array	|列表数据，用于判断是否为空，展示数据为空的提示	|
|loading|Boolean		|是否加载中，加载中的时候，会显示加载中的动画	|
|tips	|String	|当数据为空时的提示文字，默认：抱歉，暂无数据~	|

```html
<empty :list="list" :loading="loading"></empty>
```

#### 【images 组件】

>图片显示、预览组件

|属性	|类型	|说明	|
|--	|--|--	|
|width	|Number	|图片的宽度	|
|isRound|Boolean		|是否是圆形图片，否则是方形图片，默认false	|
|list	|String，Array	|要展示的图片列表，可以是图片链接数组，也可以是英文逗号分开的多个图片链接，如果是json数组，需要指定valueKey	|
|valueKey	|String	|要展示的图片数据如果是json数组，可以通过该属性指定json数据中的哪个键名为图片链接	|
|count	|Number	|要展示的图片的数量，超出数量不展示，-1为不限制，默认-1	|
|disabled	|Boolean	|是否显示右上角的删除按钮，是否可以编辑，默认false	|
|@remove	|	|当删除图片时触发回调	|

#### 【labels 组件】

>表单标题组件，主要为了对齐其他的表单布局使用

|属性	|必填	|默认值	|可选值	|示例值	|说明	|
|--	|--	|--	|--	|--	|--	|
|title	|否	|	|	|	|表单标题	|
|titleWidth	|否	|90	|数值即可	|100	|左侧标题的宽度	|
|isVertical	|否	|false	|	|	|标题和文本框是否垂直排列	|
|isTop	|否	|false	|	|	|标题与右侧是否顶部对齐，否则垂直对齐	|

#### 【layout 组件】

>布局组件，所有页面使用

|属性	|类型	|说明	|
|--	|--|--	|
|title	|String	|当前页面的标题	|
|loading|Boolean		|是否加载中，加载中的时候，会显示加载中的动画	|
|pageKey	|String	|当前页面的唯一标识，用于左侧菜单显示选中状态	|
|slot="titleLeft"	|	|标题行左侧位置的插槽	|
|slot="titleRight"	|	|标题行右侧位置的插槽	|

#### 【mores 组件】

>当文本内容为多行时，只显示一行，点击该文字，可以展示显示全部，再次点击则收起。

```html
<mores>{{item.content}}</mores>
```


#### 【paginate 组件】

>分页器组件，需要传入pageNumber（页码）属性和page（分页数据）属性。其中page属性详细结构如下，在BaseCloud的公共模块已对分页数据做了封装，直接调用即可返回该数据结构：

```js
page: {
	pageNumber: 1, //页码
	lastPage: true, //是否最后一页
	totalPage: 1, //总页码
	list: [], //列表数据
	totalRow: 0, //总数据条数
	pageSize: 10 //每页条数
},
```

>该组件会触发一个回调函数@switchPage，返回数据结构如下：

```js
{
	pageSizeChanged : true , //每页数据条数是否切换
	pageNumber :  1 , //页码
	pageSize : 5 //每页数据条数
}
```

#### 【tables 组件】

|属性	|类型	|说明	|
|--	|--|--	|
|list	|Array	|列表数据	|
|showEmpty	|Boolean	|如果列表数据为空，是否展示暂无数据提示	|
|slot="thead"	|	|表格的标题栏，无须写tr	|
|slot="tbody"	|	|表格的内容	|


```html
<tables :list="list">
	<block slot="thead">
		<th>角色名称</th>
		<th>类型</th>
		<th class="autoWidth">权限描述</th>
		<th>操作</th>
	</block>
	<block slot="tbody">
		<tr v-for="( x , index) in list" :key="index">
			<td>{{x.name}}</td>
			<td>{{x.typeStr}}</td>
			<td>{{x.remark}}</td>
			<td>
				<auth-nav :href="`/pages/role/roleEdit?id=${x._id}`" 
				url="admin/role/info" class="main bold plr5">
					编辑
				</auth-nav>
				
				<auth-btn :url="`admin/role/delete?id=${x._id}`" confirm="delete" 
				@success="remove(index)" class="main bold plr5">
					删除
				</auth-btn>
			</td>
		</tr>
	</block>
</tables>
```
 
#### 【upload-images 组件】

>图片上传组件，直接上传到云储存，支持多张图片上传

|属性	|类型	|说明	|
|--	|--|--	|
|count	|Number	|最多可以上传多少张图片，默认不限制-1	|
|name	|String	|表单的name	|
|value	|Array	|默认显示的图片列表，可以是数组，也可以是英文逗号分割的图片地址，也可以通过valueKey来指定对象类型的图片数据的图片链接键名	|
|valueKey	|String	|如果value为数组并且每个图片对应的数据是一个json对象，可以通过valueKey来指定图片的链接对应的键名。指定了valueKey后，表单提交的value将会是一个JSON字符串型的数组。|
|deleteUrl	|String	|当删除图片时，如果有配置删除的请求地址，则会向该地址发送请求，传入fileID参数，从云存储删掉该图片	|
|@change	|	|图片上传或删除时的回调，e.detail.value为图片链接数组，e.detail.files为包含图片详细信息的数组（不指定valueKey时默认图片链接为url字段）	|
|@delete	|	|图片删除成功的回调	|

```html
//指定valueKey时，需要传入的value为一个数组或字符串数组，数组中每个元素为json对象
<upload-images title="头像" name="x.avatar" valueKey="src" v-model="data.avatar" :count="1"></upload-images>
```

```js
export default {
	data() {
		return {
			data : {
				//可以为数组，也可以为字符串化的数组
				avatar : [
					{
						src : 'http://xxxx.com/xx.jpg' , //必填属性
						size : 2501 //其他选填属性...
					}
				]
			}
		}
	},
}
```

#### 【quote 组件】

>一段引用语、提示语

|属性	|类型	|说明	|
|--	|--|--	|
|title	|String	|标题	|
|color	|String	|颜色，默认主题色	|

#### 【alerts 弹窗组件】

>模态框，弹窗

|属性	|类型	|说明	|
|--	|--|--	|
|title	|String	|标题	|
|width	|String	|弹窗宽度，支持px和%单位，默认50%。	|
|height	|String	|弹窗高度，支持px和%单位，默认80%。	|
|slot="top"	|slot	|插入弹窗顶部的内容，如果仅显示标题文字不满足需要，可以自定义这块的内容	|
|slot="bottom"	|slot	|插入弹窗底部的内容，一般按钮放这里	|

参考如下代码显示弹窗：

```html
//定义弹窗的ref属性为alerts或其他自定义的名字
<alerts ref="alerts"></alerts>
```

```js
showAlerts:function(){
	this.$refs.alerts.show(); //显示弹窗
	this.refs.alerts.hide();
}
```